Closure Tools
=============

Colección de herramientas Closure de Google.

Para utilizar estas herramientas en la Build Chain por defecto, se deberá 
incluir el script

    builder-config.rb
	
en el script Rake, tras incluir la Biblioteca Rake